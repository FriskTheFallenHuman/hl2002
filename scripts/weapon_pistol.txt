// Pistol

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"	"#HL2_Pistol"
	"viewmodel"			"models/weapons/v_pistol.mdl"
	"playermodel"		"models/weapons/w_pistol.mdl"
	"anim_prefix"		"pistol"
	"bucket"			"1"
	"bucket_position"	"0"
	"bucket_360"			"0"
	"bucket_position_360"	"0"

	"clip_size"			"18"
	"primary_ammo"		"Pistol"
	"secondary_ammo"	"None"

	"weight"		"2"
	"rumble"		"1"
	"item_flags"		"0"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{

		"reload"		"Weapon_Pistol.Reload"
		"reload_npc"	"Weapon_Pistol.NPC_Reload"
		"empty"			"Weapon_Pistol.Empty"
		"single_shot"	"Weapon_Pistol.Single"
		"single_shot_npc"	"Weapon_Pistol.NPC_Single"
		"special1"		"Weapon_Pistol.Special1"
		"special2"		"Weapon_Pistol.Special2"
		"burst"			"Weapon_Pistol.Burst"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"file"		"sprites/icons"
				"x"			"250"
				"y"			"59"
				"width"		"129"
				"height"	"65"
		}
		"weapon_s"
		{	
				"file"		"sprites/icons_selected"
				"x"			"250"
				"y"			"59"
				"width"		"129"
				"height"	"65"
		}
		"weapon_small"
		{
				"file"		"sprites/icons_pickup"
				"x"			"250"
				"y"			"59"
				"width"		"129"
				"height"	"65"
		}
		"ammo"
		{
				"file"		"sprites/a_icons1"
				"x"			"55"
				"y"			"75"
				"width"		"73"
				"height"	"15"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
}