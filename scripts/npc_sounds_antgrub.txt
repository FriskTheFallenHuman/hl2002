"NPC_Antlion_Grub.Voice"
{
	"channel"		"CHAN_VOICE"
	"volume"		"0.6"
	"soundlevel"  	"SNDLVL_90dB"
	"pitch"		"90,120"
	"rndwave"
	{
		"wave"	"npc/antlion_grub/voice.wav"
		"wave"	"npc/antlion_grub/voice2.wav"
		"wave"	"npc/antlion_grub/voice3.wav"
	}
}

"NPC_Antlion_Grub.Scared"
{
	"channel"		"CHAN_VOICE"
	"volume"		"1.0"
	"soundlevel"  	"SNDLVL_100dB"
	"pitch"		"90,110"

	"rndwave"
	{
		"wave"	"npc/antlion_grub/scared1.wav"
		"wave"	"npc/antlion_grub/scared2.wav"
		"wave"	"npc/antlion_grub/scared3.wav"
	}
}

"NPC_Antlion_Grub.Health"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"  	"SNDLVL_80dB"
	"pitch"			"100,115"
	
	"wave"	"npc/antlion_grub/heal.wav"
}

"NPC_Antlion_Grub.Move"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"soundlevel"  	"SNDLVL_80dB"
	"pitch"			"100,115"
	
	"wave"	"npc/antlion_grub/movement.wav"
}

"NPC_Antlion_Grub.Squash"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"soundlevel"  	"SNDLVL_80dB"
	"pitch"			"100,115"
	
	"wave"	"npc/antlion_grub/squashed.wav"
}