// Combine Guard

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"	"#HL2_CGuard"
	"viewmodel"				"models/weapons/v_guardgun.mdl"
	"playermodel"			"models/weapons/w_guardgun.mdl"
	"anim_prefix"			"guardgun"
	"bucket"				"5"
	"bucket_position"		"2"

	"clip_size"				"1"
	"primary_ammo"			"AR2_Grenade"
	"secondary_ammo"		"None"

	"weight"				"2"
	"item_flags"			"0"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"special1"			"Weapon_CombineGuard.Special1"
		"reload"			"Weapon_CombineGuard.Reload"
		"reload_npc"		"Weapon_CombineGuard.NPC_Reload"
		"single_shot"		"Weapon_CombineGuard.Single"
		"single_shot_npc"	"Weapon_CombineGuard.NPC_Single"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"file"		"sprites/icons"
				"x"			"251"
				"y"			"370"
				"width"		"129"
				"height"	"65"
		}
		"weapon_s"
		{
				"file"		"sprites/icons_selected"
				"x"			"251"
				"y"			"370"
				"width"		"129"
				"height"	"65"
		}
		"ammo"
		{
				"file"		"sprites/a_icons1"
				"x"			"55"
				"y"			"60"
				"width"		"73"
				"height"	"15"
		}
		"ammo2"
		{
				"file"		"sprites/a_icons1"
				"x"			"55"
				"y"			"110"
				"width"		"73"
				"height"	"20"
		}
		"crosshair"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
}